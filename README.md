# vtt-utils

> Utils for handling VTT subtitles

## Install

```bash
npm i -S vtt-utils
```

## Usage

```js
var vttutils = require('vtt-utils');

// outputVttText contains one single sentence per cue from inputVttText
var outputVttText = vttutils.parseToSentences(inputVttText);

```

## License

[MIT](http://vjpr.mit-license.org)
